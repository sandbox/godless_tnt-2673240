<?php

/**
 * @file
 */

/**
 * Implements hook_theme().
 */
function flash_messages_theme() {
  return array(
    'settings_form' => array(
      'template' => 'settings-form',
      'variables' => array(
        'form' => array(),
      ),
    ),
    'bootstrap_messages' => array(
      'template' => 'bootstrap-messages',
      'variables' => array(
        'message' => array(),
      ),
    ),
    'progress_messages' => array(
      'template' => 'progress-messages',
      'variables' => array(
        'message' => array(),
      ),
    ),
    'toast_messages' => array(
      'template' => 'toast-messages',
      'variables' => array(
        'message' => array(),
      ),
    ),
    'notify_messages' => array(
      'template' => 'notify-messages',
      'variables' => array(
        'message' => array(),
      ),
    ),
    // Theme for radio group.
    'radio_buttons' => array(
      'template' => 'radio-buttons',
    ),
  );
}

/**
 * Implements template_preprocess_theme().
 */
function template_preprocess_radio_buttons(&$variables) {
  $selected_theme = selectedTheme();
  $variables['options'] = $variables['']['#options'];
  $variables['selected_theme'] = $selected_theme;
  $variables['theme_path'] = '/' . drupal_get_path('module', 'flash_messages');

  return $variables;
}



/**
 * Implements hook_preprocess_block().
 */
function flash_messages_preprocess_block(&$variables) {
  if ($variables['plugin_id'] == 'system_messages_block') {
    // Check if is enabled for user role.
    $config = \Drupal::config('flash_messages.settings');
    $config_user_roles = $config->get('roles');
    $userCurrent = \Drupal::currentUser();
    $user = Drupal\user\Entity\User::load($userCurrent->id());
    $user_roles = $user->getRoles();
    $enabled = FALSE;
    foreach ($user_roles as $value => $role) {
      foreach ($config_user_roles as $field => $config_role) {
        if ($config_user_roles[$field]['active'] == TRUE && $role == $field) {
          $enabled = TRUE;
        }
      }
    }

    // If status theme is enabled for user role.
    if ($enabled == TRUE) {
      // Disable caching for this system block.
      $variables['#cache'] = array(
        'max-age' => 0,
      );

      $config = \Drupal::config('flash_messages.settings');
      $fields = $config->get('themes');
      $selected_theme = selectedTheme();

      $messages = drupal_get_messages();
      foreach ($messages as $type => $message) {
        $flash_message = array(
          '#theme' => $selected_theme,
          '#message' => array(
            'type' => $type,
            'message' => $message[0],
          ),
        );
        $output = drupal_render($flash_message);
        $variables['content'] = $output;
        $variables['#attached']['library'][] = 'flash_messages/' . $selected_theme;
      }
    }

    return $variables;
  }
}

/**
 * Returns the selected theme.
 */
function selectedTheme() {
  $config = \Drupal::config('flash_messages.settings');
  $themes = $config->get('themes');
  $selected_theme = array_filter($themes, function($row) {
    if ($row['status'] == TRUE) {
      return $row['path'];
    }
  });
  $key = key($selected_theme);

  return $selected_theme[$key]['path'];
}
