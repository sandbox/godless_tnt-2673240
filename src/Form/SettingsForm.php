<?php
/**
 * @file
 * Contains \Drupal\flash_messages\Form\SettingsForm.
 */

namespace Drupal\flash_messages\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
/**
 *
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flash_messages_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'flash_messages.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get global config.
    $config = \Drupal::config('flash_messages.settings');

    $themes = $config->get('themes');
    $themes_options = array();
    $selected_option = NULL;

    // Themes array for form element.
    foreach ($themes as $theme => $field) {
      $themes_options[$field['path']] = $field['label'];
      if ($field['status'] == TRUE) {
        $selected_option = $theme;
      }
    }

    // All user roles array for form element.
    $user_roles = Role::loadMultiple();
    $config_user_roles = $config->get('roles');
    $user_roles_options = array();
    $user_roles_selected_options = array();
    foreach ($user_roles as $name => $entity) {
      // Check if role is set to true.
      foreach ($config_user_roles as $field => $role) {
        if ($name == $field) {
          if ($config_user_roles[$field]['active'] == TRUE) {
            $user_roles_selected_options[$field] = $field;
          }
        }
        $user_roles_options[$entity->id()] = $name;
      }
    }

    // Populating form starts here.
    $form['#attached']['library'][] = 'flash_messages/global-styling';

    $form['enabled_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enabled for the following roles:'),
      '#options' => $user_roles_options,
      '#default_value' => $user_roles_selected_options,
    );

    $form['themes'] = array(
      '#type' => 'radios',
      '#title' => t('Available themes:'),
      '#options' => $themes_options,
      '#default_value' => $selected_option,
      '#theme' => 'radio_buttons',
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('flash_messages.settings');

    // Part themes.
    $selected_theme = $form_state->getValue('themes');
    $options = $form['themes']['#options'];
    $themes = $config->get('themes');
    $arr_settings = array();
    foreach ($options as $value => $field) {
      $status = FALSE;
      if ($value == $selected_theme) {
        $status = TRUE;
      }
      $arr_settings[$value]['label'] = $field;
      $arr_settings[$value]['status'] = $status;
      $arr_settings[$value]['path'] = $themes[$value]['path'];
    }

    // Part roles.
    $selected_roles = $form_state->getValue('enabled_roles');
    $roles = $config->get('roles');
    $arr_settings_roles = array();
    foreach ($selected_roles as $field => $value) {
      $active = FALSE;
      if ($value !== 0) {
        $active = TRUE;
      }
      $arr_settings_roles[$field]['active'] = $active;
    }

    $this->config('flash_messages.settings')
      ->set('themes', array_merge($themes, $arr_settings))
      ->set('roles', array_merge($roles, $arr_settings_roles))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
