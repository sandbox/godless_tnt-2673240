<?php
/**
 * @file
 * Contains \Drupal\flash_messages\Controller\FlashMessagesController.
 */

namespace Drupal\flash_messages\Controller;

use Drupal\Core\Controller\ControllerBase;
/**
 *
 */
class FlashMessagesController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $form = \Drupal::formBuilder()->getForm('Drupal\flash_messages\Form\Settingsform');

    $build = array(
      '#title' => t('Flash Messages'),
      '#theme' => 'settings_form',
      '#form' => $form,
    );

    return $build;
  }

}
