(function ($) {
  // When clicking theme image, check radio button
  $(".flash-messages-theme-image").click(function() {
    $(this).parent('div').children('input[type=radio]').prop("checked", true);
  });
})(jQuery);