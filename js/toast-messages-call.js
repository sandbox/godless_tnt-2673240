(function ($) {
  Drupal.behaviors.flash_messages = {
    attach: function (context, settings) {

    String.prototype.capitalize = function() {
      return this.charAt(0).toUpperCase() + this.slice(1);
    }
		// Sticky version
   var $status = $('.flash-messages-type').html();
   var $message = $('.flash-messages-message').html();
		$.toast({ 
		  heading: $status.capitalize(),
      text: $message,
      icon: $status,
      "closeButton": true,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "10000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
		})


    }
  };
})(jQuery);