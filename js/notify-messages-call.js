(function ($) {
  Drupal.behaviors.flash_messages = {
    attach: function (context, settings) {

      String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
      }
      var $status = $('.flash-messages-type').html();
      var $message = $('.flash-messages-message').html();
      $(this).notifyMe(
        'bottom',
        $status,
        $status.capitalize(),
        $message,
        200,
        10000
      ); 
    }
  };
})(jQuery);