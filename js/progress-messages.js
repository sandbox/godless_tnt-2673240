(function ($) {
	// When message is clicked, hide it.
	$('.flash-message-progress-theme').click(function(){		
	    $(this).slideUp(500, function() {
			$(this).remove();
		});
	});		 
		 
})(jQuery);   